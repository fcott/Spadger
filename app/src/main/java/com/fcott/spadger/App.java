package com.fcott.spadger;

import android.app.Activity;
import android.app.Application;
import android.os.Process;

import com.fcott.spadger.utils.netstatus.NetStateReceiver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/12/27.
 */

public class App extends Application {
    public static final String APP_ID = "3802f3bd93"; // TODO 替换成bugly上注册的appid
    public static final String APP_CHANNEL = "DEBUG"; // TODO 自定义渠道
    public ArrayList<String> perLoadList = new ArrayList<>();

    private static App instance;
    private List<Activity> activityList;

    @Override
    public void onCreate() {
        super.onCreate();

        NetStateReceiver.registerNetworkStateReceiver(this);
        instance = this;
        activityList = new ArrayList<>();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        NetStateReceiver.unRegisterNetworkStateReceiver(this);
    }

    public static App getInstance() {
        return instance;
    }

    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    public void cleanActivity(boolean needKillProcess) {
        for (Activity activitie : activityList) {
            if (activitie != null) {
                activitie.finish();
            }
        }
        if(needKillProcess)
            Process.killProcess(Process.myPid());
    }
    public void cleanActivity() {
        cleanActivity(true);
    }

}
