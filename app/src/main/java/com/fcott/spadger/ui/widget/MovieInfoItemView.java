package com.fcott.spadger.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fcott.spadger.R;
import com.fcott.spadger.utils.MMHScreenParameterUtil;

public class MovieInfoItemView extends AppCompatTextView{

    private static final int dp5 = MMHScreenParameterUtil.dip2px(5);
    private static final int dp2 = MMHScreenParameterUtil.dip2px(2);

    public MovieInfoItemView(Context context) {
        super(context);
        init("");
    }

    public MovieInfoItemView(Context context,String name) {
        super(context);
        init(name);
    }

    public MovieInfoItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init("");
    }

    public MovieInfoItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init("");
    }

    private void init(String name){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = dp5;
        layoutParams.rightMargin = dp5;
        layoutParams.bottomMargin = dp2;
        setLayoutParams(layoutParams);
        setPadding(dp5, dp5, dp5, dp5);
        setBackgroundResource(R.drawable.selector_btn);
        setText(name);
    }
}
