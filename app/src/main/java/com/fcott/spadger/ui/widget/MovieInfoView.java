package com.fcott.spadger.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fcott.spadger.R;
import com.fcott.spadger.model.bean.DataBean;
import com.google.android.flexbox.FlexboxLayout;

import java.util.List;

public class MovieInfoView extends LinearLayout{

    TextView textView;
    FlexboxLayout flexboxLayout;
    private OnSelectListener onSelectListener;

    public MovieInfoView(Context context) {
        super(context);
        init();
    }

    public MovieInfoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MovieInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        setOrientation(HORIZONTAL);
        View.inflate(getContext(), R.layout.widget_movie_info_item, this);
        textView = findViewById(R.id.tv_name);
        flexboxLayout = findViewById(R.id.flexboxlayout);
    }

    public void initData(String title,List<DataBean> dataList){

        if(TextUtils.isEmpty(title) || dataList == null || dataList.size() == 0){
            setVisibility(GONE);
            return;
        }

        textView.setText(title);
        for(DataBean bean:dataList){
            if(bean == null || TextUtils.isEmpty(bean.getName()))
                continue;
            MovieInfoItemView textView = new MovieInfoItemView(getContext(),bean.getName());
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onSelectListener != null){
                        onSelectListener.onSelect(bean.getID(),bean.getName());
                    }
                }
            });
            flexboxLayout.addView(textView);
        }
    }

    public void setOnSelectListener(OnSelectListener onSelectListener){
        this.onSelectListener = onSelectListener;
    }

    public interface OnSelectListener{
        void onSelect(int id,String name);
    }


}
