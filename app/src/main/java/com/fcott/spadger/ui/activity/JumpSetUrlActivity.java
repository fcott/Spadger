package com.fcott.spadger.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.fcott.spadger.Config;
import com.fcott.spadger.ui.activity.look.LookMovieActivity;
import com.fcott.spadger.utils.ProgressBarUtils;

public class JumpSetUrlActivity extends Activity {

    private Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProgressBarUtils.showProgressBar(this);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Config.isJump = true;
                startActivity(getIntent().setClass(JumpSetUrlActivity.this, DisclaimerActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));
                ProgressBarUtils.hideProgressBar(JumpSetUrlActivity.this);
                finish();
            }
        }, 300);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
