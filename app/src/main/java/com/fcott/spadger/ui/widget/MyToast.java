package com.fcott.spadger.ui.widget;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fcott.spadger.App;
import com.fcott.spadger.R;
import com.fcott.spadger.utils.LogUtil;
import com.fcott.spadger.utils.SafelyHandlerWarpper;

import java.lang.reflect.Field;



/***
 * 自定义简单通用toast
 */
public class MyToast extends Toast {

    private TextView text;

    private Context mContext;
    /**
     * 之前显示的内容
     */
    private static String oldMsg;
    /**
     * Toast对象
     */
    private static MyToast toast = null;
    /**
     * 第一次时间
     */
    private static long oneTime = 0;
    /**
     * 第二次时间
     */
    private static long twoTime = 0;

    public MyToast(Context mContext) {
        super(mContext);
        this.mContext = mContext;
        View viewGroup = LayoutInflater.from(mContext).inflate(R.layout.toast, null, false);
        text = (TextView) viewGroup.findViewById(R.id.tv_toast);
        this.setView(viewGroup);
    }

    @Override
    public void setText(@NonNull CharSequence s) {
        text.setText(s);
    }

    @Override
    public void setText(@NonNull int resId) {
        text.setText(mContext.getString(resId));
    }

    @Deprecated
    public static void showToast(@NonNull Context mContext, @NonNull CharSequence s) {
        showToast(s);
    }

    @Deprecated
    public static void showToast(@NonNull Context mContext, @NonNull int resId) {
        showToast(resId);
    }

    public static void showToast(CharSequence s) {
        if (TextUtils.isEmpty(s)) return;
        Context context = App.getInstance();
        if (null == context) return;
        showToastDetail(context, s);
    }

    public static void showToast(int resId) {
        if (resId == 0) return;
        Context context = App.getInstance();
        if (null == context) return;
        showToastDetail(context, context.getResources().getString(resId));
    }

    private static void showToastDetail(Context context, CharSequence s) {
        try {
            if (toast == null) {
                toast = new MyToast(context);
                toast.setText(s);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                oneTime = System.currentTimeMillis();
            } else {
                twoTime = System.currentTimeMillis();
                if (s.toString().equals(oldMsg)) {
                    if (twoTime - oneTime > Toast.LENGTH_SHORT) { // 实际上这一行没有意义，但是现在不知道怎么改好，为了保证稳定性，就没改
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.setText(s.toString());
                        toast.show();
                    }
                } else {
                    oldMsg = s.toString();
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.setText(s.toString());
                    toast.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        oneTime = twoTime;
    }

    public static void cancels() {
        if (null != toast)
            toast.cancel();
    }

    @Override
    public void show() {
        //判断应用是否处于前台
        if (isForeground(mContext)) {
            if (Build.VERSION.SDK_INT < 28) {
                hook(toast);
            }
            super.show();
        }
    }

    private static Field sField_TN ;
    private static Field sField_TN_Handler ;
    static {
        try {
            sField_TN = Toast.class.getDeclaredField("mTN");
            sField_TN.setAccessible(true);
            sField_TN_Handler = sField_TN.getType().getDeclaredField("mHandler");
            sField_TN_Handler.setAccessible(true);
        } catch (Exception e) {}
    }

    private static void hook(Toast toast) {
        try {
            Object tn = sField_TN.get(toast);
            Handler preHandler = (Handler)sField_TN_Handler.get(tn);
            sField_TN_Handler.set(tn,new SafelyHandlerWarpper(preHandler));
        } catch (Exception e) {}
    }

    /**
     * 应用是否处于前台
     *
     * @param context
     * @return
     */
    private static boolean isForeground(Context context) {
        if (context != null) {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
            String currentPackageName = cn.getPackageName();
            if (!TextUtils.isEmpty(currentPackageName) && currentPackageName.equals(context.getPackageName())) {
                return true;
            }
            return false;
        }
        return false;
    }
}