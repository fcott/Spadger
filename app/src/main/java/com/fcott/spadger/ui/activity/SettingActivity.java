package com.fcott.spadger.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.fcott.spadger.R;
import com.fcott.spadger.ui.activity.look.MovieDetialActivity;
import com.fcott.spadger.ui.widget.AlertDialogFragment;
import com.fcott.spadger.ui.widget.MyToast;
import com.fcott.spadger.utils.ACache;
import com.fcott.spadger.utils.AsyncUtils;
import com.fcott.spadger.utils.FileUtil;
import com.fcott.spadger.utils.GeneralSettingUtil;
import com.fcott.spadger.utils.db.DBManager;
import com.fcott.spadger.utils.db.DatabaseHelper;
import com.fcott.spadger.utils.imageUtils.ImageCacheManager;

import butterknife.Bind;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {

    @Bind(R.id.switch_prohibit_no_wifi)
    public Switch switchProhibitNoWifi;
    @Bind(R.id.switch_perload)
    public Switch switchPerLoad;
    @Bind(R.id.switch_cancel_tip)
    public Switch switchCancelTip;

    @OnClick({R.id.clearBrowseHistory,R.id.clearCache})
    public void click(View view){
        switch (view.getId()){
            case R.id.clearBrowseHistory:
                new AlertDialogFragment().setMessage("確定清除瀏覽歷史?").setSelectFromListener(new AlertDialogFragment.SelectFromListener() {
                    @Override
                    public void positiveClick() {
                        DBManager dbManager = new DBManager(SettingActivity.this);
                        dbManager.clearTable(DatabaseHelper.RECORD_TABLE);
                        dbManager.closeDB();
                        MyToast.showToast("清除成功");
                    }

                    @Override
                    public void negativeClick() {

                    }
                }).show(getFragmentManager(),"record");
                break;
            case R.id.clearCache:
                new AlertDialogFragment().setMessage("確定清除APP緩存?").setSelectFromListener(new AlertDialogFragment.SelectFromListener() {
                    @Override
                    public void positiveClick() {
                        AsyncUtils.async(new AsyncUtils.AsyncObjecyer() {
                            @Override
                            public void doAsyncAction() {
                                try {
                                    ImageCacheManager.clearDiskCache(SettingActivity.this);
                                    ACache aCache = ACache.get(SettingActivity.this);
                                    aCache.clear();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                        ImageCacheManager.clearMemoryCache(SettingActivity.this);
                        MyToast.showToast("清除成功");
                    }

                    @Override
                    public void negativeClick() {

                    }
                }).show(getFragmentManager(),"cache");
                break;
        }
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_setting;
    }

    @Override
    protected void getBundleExtras(Bundle bundle) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void initViews() {

        ActionBar mActionBar=getSupportActionBar();
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setTitle("设置");

        switchProhibitNoWifi.setChecked(GeneralSettingUtil.isProhibitNoWifi());
        switchProhibitNoWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                GeneralSettingUtil.setProhibitNoWifi(isChecked);
            }
        });


        switchPerLoad.setChecked(GeneralSettingUtil.isPerLoad());
        switchPerLoad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                GeneralSettingUtil.setPerLoad(isChecked);
            }
        });

        switchCancelTip.setChecked(GeneralSettingUtil.isCancelTip());
        switchCancelTip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                GeneralSettingUtil.setCancelTip(isChecked);
            }
        });
    }

}
