package com.fcott.spadger.ui.activity;

import android.os.Bundle;
import com.fcott.spadger.R;

public class ExplainActivity extends BaseActivity {

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_explain;
    }

    @Override
    protected void getBundleExtras(Bundle bundle) {

    }

    @Override
    protected void initViews() {

    }
}
