package com.fcott.spadger.ui.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.fcott.spadger.R;
import com.fcott.spadger.model.bean.MovieBean;
import com.fcott.spadger.ui.activity.look.MovieDetialActivity;
import com.fcott.spadger.ui.adapter.baseadapter.BaseAdapter;
import com.fcott.spadger.ui.adapter.baseadapter.ViewHolder;
import com.fcott.spadger.utils.MMHScreenParameterUtil;
import com.fcott.spadger.utils.imageUtils.ImageCacheManager;
import com.fcott.spadger.utils.imageUtils.bean.SimpleImageOpConfiger;

import java.util.List;

/**
 * Created by Administrator on 2017/8/11.
 */

public class MovieListAdapter extends BaseAdapter<MovieBean.MessageBean.MoviesBean> {
    private Context context;
    private SimpleImageOpConfiger simpleImageOpConfiger;

    public MovieListAdapter(Context context, List<MovieBean.MessageBean.MoviesBean> datas, boolean isOpenLoadMore) {
        super(context, datas, isOpenLoadMore);
        this.context = context;
        simpleImageOpConfiger = new SimpleImageOpConfiger();
        simpleImageOpConfiger.thumbnail = 0.3f;
    }

    @Override
    protected void convert(ViewHolder holder, MovieBean.MessageBean.MoviesBean data) {
        ImageView imageView = holder.getView(R.id.img_cover);
        TextView title = holder.getView(R.id.tv_title);

        title.setText(data.getName());

        ImageCacheManager.showImage(context, data.getCoverImg(), imageView, MMHScreenParameterUtil.getScreenWidth(),
                MMHScreenParameterUtil.dip2px(290), simpleImageOpConfiger);
    }

    @Override
    protected int getItemLayoutId() {
        return R.layout.item_movie;
    }
}
