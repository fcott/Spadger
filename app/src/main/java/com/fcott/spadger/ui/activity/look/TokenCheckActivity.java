package com.fcott.spadger.ui.activity.look;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fcott.spadger.App;
import com.fcott.spadger.Config;
import com.fcott.spadger.R;
import com.fcott.spadger.model.bean.LoginBean;
import com.fcott.spadger.model.http.LookMovieService;
import com.fcott.spadger.model.http.utils.RetrofitUtils;
import com.fcott.spadger.ui.activity.BaseActivity;
import com.fcott.spadger.utils.DeviceUuidFactory;
import com.fcott.spadger.utils.FileUtil;
import com.fcott.spadger.utils.GeneralSettingUtil;
import com.fcott.spadger.utils.NativeUtil;
import com.fcott.spadger.utils.ProgressBarUtils;

import java.io.File;

import butterknife.Bind;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TokenCheckActivity extends BaseActivity {
    public static final String TAG = TokenCheckActivity.class.getSimpleName();
    public static final String FROM = TAG+"FROM";

    private RequestBody loginBody;

    @Bind(R.id.contain)
    View contain;
    @Bind(R.id.tv_token)
    EditText editText;
    @Bind(R.id.btn_confirm)
    Button btnConfirm;
    @Bind(R.id.ll_install)
    LinearLayout llInstall;

    @Override
    protected View getLoadingTargetView() {
        return contain;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_token_check;
    }

    @Override
    protected void getBundleExtras(Bundle bundle) {
        String token = bundle.getString("token");
        if(!TextUtils.isEmpty(token)){
            try {
                String ts[] = NativeUtil.decrypt(token).split(Config.DELIMITER2);
                GeneralSettingUtil.setEndTime(ts[1]);
                token = ts[0];
            }catch (Exception e){
                Toast.makeText(TokenCheckActivity.this,"未知錯誤,請重新獲取驗證碼",Toast.LENGTH_SHORT).show();
                return;
            }

            login(token);
        }
    }

    @Override
    protected void initViews() {
        if(!NativeUtil.isAppInstalled(Config.PATH) && Config.showInstallType.equals("1")){
            llInstall.setVisibility(View.VISIBLE);
        }else {
            llInstall.setVisibility(View.GONE);
        }
    }

    public void login(View view) {
        String token = editText.getText().toString().trim();
        if(TextUtils.isEmpty(token)){
            Toast.makeText(TokenCheckActivity.this,"請輸入驗證碼",Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            String ts[] = NativeUtil.decrypt(token).split(Config.DELIMITER2);
            GeneralSettingUtil.setEndTime(ts[1]);
            token = ts[0];
        }catch (Exception e){
            Toast.makeText(TokenCheckActivity.this,"未知錯誤,請重新獲取驗證碼",Toast.LENGTH_SHORT).show();
            return;
        }

        login(token);
    }

    private void login(String token){
//        toggleShowLoading(true);
        ProgressBarUtils.showProgressBar(this);
        loginBody = new FormBody.Builder()
                .add("Token", token)
                .build();
        RetrofitUtils.getInstance().create(LookMovieService.class)
                .login(loginBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LoginBean>() {
                    @Override
                    public void onCompleted() {
                        Log.w("response", "completed");
                    }

                    @Override
                    public void onError(Throwable e) {
//                        toggleShowLoading(false);
                        ProgressBarUtils.hideProgressBar(TokenCheckActivity.this);
                        Toast.makeText(TokenCheckActivity.this,"驗證失敗",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(LoginBean loginBean) {
//                        toggleShowLoading(false);
                        ProgressBarUtils.hideProgressBar(TokenCheckActivity.this);
                        if (loginBean.getResult() == 1) {
                            finish();
                            Toast.makeText(TokenCheckActivity.this,"驗證成功",Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TokenCheckActivity.this,"驗證失敗",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void install(View view){
        FileUtil.installApk();
        File f = new File(App.getInstance().getCacheDir().getPath() + "/aisheng.apk");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            //判读版本是否在7.0以上
            Uri apkUri = FileProvider.getUriForFile(this, "com.fcott.spadger.fileProvider", f);//在AndroidManifest中的android:authorities值
            Intent install = new Intent(Intent.ACTION_VIEW);
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            install.setDataAndType(apkUri, "application/vnd.android.package-archive");
            startActivity(install);
        }else{
            //以前的启动方法
            Intent install = new Intent(Intent.ACTION_VIEW);
            install.setDataAndType(Uri.fromFile(f), "application/vnd.android.package-archive");
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(install);
        }
    }

//    public void autoLogin(String token) {
//        toggleShowLoading(true);
//        loginBody = new FormBody.Builder()
//                .add("Token", token)
//                .build();
//        RetrofitUtils.getInstance().create(LookMovieService.class)
//                .login(loginBody)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<LoginBean>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.w("response", "completed");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        toggleShowError("请求出错,点击重试", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                initViews();
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onNext(LoginBean loginBean) {
//                        if (loginBean.getResult() == 1) {
//                            toggleShowLoading(false);
//                            startActivity(new Intent(TokenCheckActivity.this, LookMovieActivity.class));
//                            finish();
//                        } else {
//                            requestToken();
//                        }
//                    }
//                });
//    }
//
//    public void requestToken() {
//        toggleShowLoading(true);
//        loginBody = new FormBody.Builder()
//                .add("data", "{\"Action\":\"CreateToken\",\"Message\":{\"UID\":\"866693021858191\"}}")
//                .build();
//        RetrofitUtils.getInstance().create(RequestTokenService.class)
//                .requestToken(loginBody)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<TokenResultBean>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        toggleShowError("请求出错,点击重试", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                initViews();
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onNext(TokenResultBean tokenResultBean) {
//                        if (tokenResultBean.getResult() == 1) {
//                            SharedPreferences.Editor sharedata = getSharedPreferences(Config.SP_TOKEN, Context.MODE_PRIVATE).edit();
//                            sharedata.putString("token", tokenResultBean.getMessage().getToken());
//                            sharedata.putString("endTime", tokenResultBean.getMessage().getEndTime());
//                            sharedata.commit();
//                            autoLogin(tokenResultBean.getMessage().getToken());
//                        } else {
//                            toggleShowError("请求出错,点击重试", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    initViews();
//                                }
//                            });
//                        }
//                    }
//                });
//
//    }
}
