package com.fcott.spadger.ui.activity.look;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.fcott.spadger.Config;
import com.fcott.spadger.R;
import com.fcott.spadger.model.bean.Data2Source;
import com.fcott.spadger.model.bean.DataBean;
import com.fcott.spadger.model.bean.MovieBean;
import com.fcott.spadger.model.bean.MovieInfoBean;
import com.fcott.spadger.model.bean.MoviePlayBean;
import com.fcott.spadger.model.http.LookMovieService;
import com.fcott.spadger.model.http.utils.RetrofitUtils;
import com.fcott.spadger.ui.activity.BaseActivity;
import com.fcott.spadger.ui.widget.MovieInfoView;
import com.fcott.spadger.utils.GsonUtil;
import com.fcott.spadger.utils.LogUtil;
import com.fcott.spadger.utils.db.DBManager;
import com.fcott.spadger.utils.db.DatabaseHelper;
import com.fcott.spadger.utils.imageUtils.ImageCacheManager;
import com.google.android.exoplayer2.source.MediaSource;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import chuangyuan.ycj.videolibrary.video.ExoUserPlayer;
import chuangyuan.ycj.videolibrary.video.VideoPlayerManager;
import chuangyuan.ycj.videolibrary.whole.WholeMediaSource;
import chuangyuan.ycj.videolibrary.widget.VideoPlayerView;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MovieDetialActivity extends BaseActivity {
    public static final String TAG = MovieDetialActivity.class.getSimpleName();

    private MovieBean.MessageBean.MoviesBean moviesBean;
    private Subscription subscription1,subscription2,subscription3;
    private boolean isFree = false;

    private ExoUserPlayer exoPlayerManager;

    @Bind(R.id.exo_play_context_id)
    public VideoPlayerView videoPlayerView;
    @Bind(R.id.tv_time)
    public TextView tvTime;
    @Bind(R.id.mv_actor)
    public MovieInfoView tvActor;
    @Bind(R.id.mv_channel)
    public MovieInfoView tvChannel;
    @Bind(R.id.mv_class)
    public MovieInfoView tvClass;
    @Bind(R.id.mv_supplier)
    public MovieInfoView tvSupplier;
    @Bind(R.id.tv_title)
    public TextView tvTitle;
    @Bind(R.id.tv_descript)
    public TextView tvDescript;
    @Bind(R.id.iv_actor)
    public ImageView ivActor;
    @Bind(R.id.iv_cut)
    public ImageView ivCut;
    @Bind(R.id.nest)
    View view;

    @Override
    protected View getLoadingTargetView() {
        return view;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_movie_detial;
    }

    @Override
    protected void getBundleExtras(Bundle bundle) {
        moviesBean = (MovieBean.MessageBean.MoviesBean) bundle.getSerializable("DATA");
        isFree = bundle.getBoolean("isFree",false);
        DBManager dbManager = new DBManager(MovieDetialActivity.this, DatabaseHelper.RECORD_TABLE);
        if(dbManager.hasContainId(moviesBean.getMovieID())){
            dbManager.deleteMovie(moviesBean.getMovieID());
        }
        dbManager.add(moviesBean);
        dbManager.closeDB();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void initViews() {
        initActionBar();
        toggleShowLoading(true);
        tvTime.setText(moviesBean.getCreateTime());
        tvDescript.setText(moviesBean.getDescription());
        tvTitle.setText(moviesBean.getName());

        RequestBody playBody = new FormBody.Builder()
                .add("MovieID", moviesBean.getMovieID())
                .build();
        RetrofitUtils.getInstance().create(LookMovieService.class)
                .requestMovieInfo(playBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MovieInfoBean>() {
                    @Override
                    public void onCompleted() {
                        unsubscribe();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(MovieInfoBean movieInfoBean) {
                        LogUtil.log(GsonUtil.toJson(movieInfoBean));
                        initBaseMovieInfo(movieInfoBean);
                    }
                });

        Observable<MoviePlayBean> observable;
        if(isFree){
            observable = RetrofitUtils.getInstance().create(LookMovieService.class)
                    .movieFreePlay(playBody);
        }else {
            observable =RetrofitUtils.getInstance().create(LookMovieService.class)
                    .moviePlay(playBody);
        }
        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MoviePlayBean>() {
                    @Override
                    public void onCompleted() {
                        unsubscribe();
                    }

                    @Override
                    public void onError(Throwable e) {
                        toggleShowLoading(false);
                        toggleShowError("请求出现错误");
                    }

                    @Override
                    public void onNext(MoviePlayBean playBean) {
                        initExoPlayerManager(moviesBean.getName(),playBean.getMessage());
                        ImageCacheManager.showImage(MovieDetialActivity.this, moviesBean.getCoverImg(), exoPlayerManager.getPreviewImage());
                        toggleShowLoading(false);
                    }
                });
    }

    private void initBaseMovieInfo(MovieInfoBean movieInfoBean){
        MovieInfoBean.MessageBean messageBean = movieInfoBean.getMessage();
        tvActor.initData("演员:",makeActor(messageBean.getActor()));
        tvChannel.initData("频道:", makeChannel(messageBean.getChannel()));
        tvClass.initData("类型:",makeClass(messageBean.getClassBean()));
        tvSupplier.initData("厂商:",makeSupplier(messageBean.getSupplier()));
        tvTime.setText(messageBean.getCreateTime());
        ImageCacheManager.showImage(MovieDetialActivity.this, messageBean.getImg(), ivActor);
        ImageCacheManager.showImage(MovieDetialActivity.this, messageBean.getCutPicName(), ivCut);
        tvActor.setOnSelectListener(new MovieInfoView.OnSelectListener() {
            @Override
            public void onSelect(int id, String name) {
                Intent intent = new Intent();
                intent.setClass(MovieDetialActivity.this, MovieListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("ACTORID", String.valueOf(id));
                bundle.putString("TYPE", Config.typeActor);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        tvChannel.setOnSelectListener(new MovieInfoView.OnSelectListener() {
            @Override
            public void onSelect(int id, String name) {
                Intent intent = new Intent();
                intent.setClass(MovieDetialActivity.this, MovieListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("ID", String.valueOf(id));
                bundle.putString("TYPE", Config.typeChannel);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        tvClass.setOnSelectListener(new MovieInfoView.OnSelectListener() {
            @Override
            public void onSelect(int id, String name) {
                Intent intent = new Intent();
                intent.setClass(MovieDetialActivity.this, MovieListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("CLASSID", String.valueOf(id));
                bundle.putString("TYPE", Config.typeClass);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        tvSupplier.setOnSelectListener(new MovieInfoView.OnSelectListener() {
            @Override
            public void onSelect(int id, String name) {
                Intent intent = new Intent();
                intent.setClass(MovieDetialActivity.this, MovieListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("SUPPLIER", String.valueOf(id));
                bundle.putString("TYPE", Config.typeSupplier);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    /**
     * 初始化ActionBar
     */
    private void initActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setTitle("电影详情");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (moviesBean == null)
            return true;

        getMenuInflater().inflate(R.menu.out_map_menu, menu);
        final Switch switchShop = (Switch) menu.findItem(R.id.myswitch).getActionView().findViewById(R.id.switchForActionBar);

        final DBManager dbManager = new DBManager(this);
        if (dbManager.hasContainId(moviesBean.getMovieID())) {
            switchShop.setChecked(true);
            switchShop.setText(getResources().getString(R.string.cancel_collection));
        }
        dbManager.closeDB();
        switchShop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton btn, boolean isChecked) {
                if (isChecked) {
                    setCollection(moviesBean, true);
                    switchShop.setText(getResources().getString(R.string.cancel_collection));
                } else {
                    setCollection(moviesBean, false);
                    switchShop.setText(getResources().getString(R.string.collection));
                }
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setCollection(final MovieBean.MessageBean.MoviesBean moviesBean, boolean isCollection) {
        DBManager dbManager = new DBManager(MovieDetialActivity.this);
        if (isCollection) {
            dbManager.add(moviesBean);
        } else {
            dbManager.deleteMovie(moviesBean.getMovieID());
        }
        dbManager.closeDB();
    }

    private List<DataBean> makeChannel(MovieInfoBean.MessageBean.ChannelBean bean) {
        ArrayList<DataBean> dataBeans = new ArrayList<>();
        dataBeans.add(new DataBean(bean.getID(),bean.getName()));
        return dataBeans;
    }

    private List<DataBean> makeActor(List<MovieInfoBean.MessageBean.ActorBean> actorBeens) {
        ArrayList<DataBean> dataBeans = new ArrayList<>();
        for(MovieInfoBean.MessageBean.ActorBean bean:actorBeens){
            dataBeans.add(new DataBean(bean.getID(),bean.getName()));
        }
        return dataBeans;
    }

    private List<DataBean> makeSupplier(MovieInfoBean.MessageBean.SupplierBean bean) {
        ArrayList<DataBean> dataBeans = new ArrayList<>();
        dataBeans.add(new DataBean(bean.getID(),bean.getName()));
        return dataBeans;
    }

    private List<DataBean> makeClass(List<MovieInfoBean.MessageBean.ClassBean> classBeen) {
        ArrayList<DataBean> dataBeans = new ArrayList<>();
        for(MovieInfoBean.MessageBean.ClassBean bean:classBeen){
            dataBeans.add(new DataBean(bean.getID(),bean.getName()));
        }
        return dataBeans;
    }

    public void initExoPlayerManager(String title,String url){
        //实例化
        WholeMediaSource wholeMediaSource =  new WholeMediaSource(this, new Data2Source(getApplication()));
        MediaSource videoSource = wholeMediaSource.initMediaSource(Uri.parse(url));
        wholeMediaSource.setMediaSource(videoSource);
        exoPlayerManager = new VideoPlayerManager.Builder(VideoPlayerManager.TYPE_PLAY_MANUAL, videoPlayerView)
                .setDataSource(wholeMediaSource)
                .setTitle(title)
                .setPlayerGestureOnTouch(true)
                .create();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(exoPlayerManager != null){
            exoPlayerManager.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(exoPlayerManager != null){
            exoPlayerManager.onPause();
        }
    }

    @Override
    public void onBackPressed() {
        if(exoPlayerManager == null){
            super.onBackPressed();
            return;
        }
        if (exoPlayerManager.onBackPressed()) {
            ActivityCompat.finishAfterTransition(this);
            exoPlayerManager.onDestroy();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(exoPlayerManager != null){
            exoPlayerManager.onDestroy();
        }
        if(subscription1 != null && subscription1.isUnsubscribed()){
            subscription1.unsubscribe();
            subscription1 = null;
        }
        if(subscription2 != null && subscription2.isUnsubscribed()){
            subscription2.unsubscribe();
            subscription2 = null;
        }
        if(subscription3 != null && subscription3.isUnsubscribed()){
            subscription3.unsubscribe();
            subscription3 = null;
        }
    }

}
