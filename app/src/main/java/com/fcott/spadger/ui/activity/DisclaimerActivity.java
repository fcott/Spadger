package com.fcott.spadger.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fcott.spadger.App;
import com.fcott.spadger.Config;
import com.fcott.spadger.R;
import com.fcott.spadger.model.bean.ActorBean;
import com.fcott.spadger.model.http.ConfigService;
import com.fcott.spadger.model.http.LookMovieService;
import com.fcott.spadger.model.http.utils.RetrofitUtils;
import com.fcott.spadger.ui.activity.look.LookMovieActivity;
import com.fcott.spadger.ui.widget.MyToast;
import com.fcott.spadger.utils.GeneralSettingUtil;
import com.fcott.spadger.utils.PermissionsChecker;
import com.google.common.base.Strings;

import java.io.IOException;
import java.util.List;

import butterknife.Bind;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DisclaimerActivity extends BaseActivity {

    public static final int PERMISSION_REQUEST_CODE = 0;                                  // 权限请求码
    private String [] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
    private PermissionsChecker mPermissionsChecker = null;// 权限检测器
    private String excludeCity = "哈尔滨长春沈阳石家庄郑州太原济南南京南昌长沙武汉昆明贵阳成都福州重庆天津上海北京拉萨乌鲁木齐海口广州北京银川呼和浩特深圳雄安";//杭州

    //0 开放注册 1不开放注册 但是可跳转 2不开放注册且不可跳转

    @Bind(R.id.sv_able)
    public ScrollView svAble;
    @Bind(R.id.ll_unable)
    public LinearLayout llUnable;

    String latLongString;
    private LocationManager locationManager;
    private double latitude = 0;

    private double longitude = 0;

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            double[] data = (double[]) msg.obj;
            List<Address> addList = null;
            Geocoder ge = new Geocoder(getApplicationContext());
            try {
                addList = ge.getFromLocation(data[0], data[1], 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addList != null && addList.size() > 0) {
                for (int i = 0; i < addList.size(); i++) {
                    Address ad = addList.get(i);
                    latLongString = ad.getLocality();
                }
            }
            if(latLongString == null){
                MyToast.showToast("請檢查網絡狀況是否良好");
                return false;
            }
            String realName = latLongString.replace("市","").replace("自治区","");
            if(excludeCity.contains(realName) || realName.contains("北京")){
                svAble.setVisibility(View.GONE);
                llUnable.setVisibility(View.VISIBLE);
                SharedPreferences sp = getSharedPreferences("config", 0);
                sp.edit().putString("ProhibitType", "3").commit();
                return false;
            }
            SharedPreferences sp = getSharedPreferences("config", 0);
            String prohibitType = sp.getString("ProhibitType", "");
            if(!prohibitType.equals("3")){
                requestProhibit();
            }else {
                svAble.setVisibility(View.GONE);
                llUnable.setVisibility(View.VISIBLE);
            }
            return false;
        }
    });

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_disclaimer;
    }

    @Override
    protected void getBundleExtras(Bundle bundle) {

    }

    @Override
    protected void initViews() {
        mPermissionsChecker = new PermissionsChecker(this);//权限检测器

        //进入权限配置页面
        if (mPermissionsChecker.lacksPermissions(permissions)) {
            PermissionsActivity.startActivityForResult(this, PERMISSION_REQUEST_CODE, permissions);
            return;
        }
        location();
    }

    public void noEighteen(View view) {
        finish();
    }

    public void agreeAgreement(View view) {
        startActivity(new Intent(this, LookMovieActivity.class));
        finish();
    }

    public void retry(View view){
        //进入权限配置页面
        if (mPermissionsChecker.lacksPermissions(permissions)) {
            PermissionsActivity.startActivityForResult(this, PERMISSION_REQUEST_CODE, permissions);
            return;
        }
        location();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 拒绝时, 关闭页面, 缺少主要权限, 无法运行
        if (requestCode == PERMISSION_REQUEST_CODE && resultCode == PermissionsActivity.PERMISSIONS_DENIED) {
            finish();
            return;
        }else if(requestCode == PERMISSION_REQUEST_CODE && resultCode == PermissionsActivity.PERMISSIONS_GRANTED){
            location();
        }
    }

    private void location(){
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        new Thread() {
            @Override
            public void run() {
                if (ActivityCompat.checkSelfPermission(DisclaimerActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DisclaimerActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude(); // 经度
                    longitude = location.getLongitude(); // 纬度
                    double[] data = {latitude, longitude};
                    Message msg = handler.obtainMessage();
                    msg.obj = data;
                    handler.sendMessage(msg);
                }
            }
        }.start();
    }

    private void requestProhibit(){
        RetrofitUtils.getInstance().create1(ConfigService.class)
                .getConfig()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        finish();
                    }

                    @Override
                    public void onNext(String string) {
                        String [] configs = string.split("aaa");
                        if(configs != null && configs.length != 2){
                            finish();
                            return;
                        }else {
                            Config.aclType = configs[0];
                            Config.showInstallType = configs[1];
                        }
                        SharedPreferences sp = getSharedPreferences("config", 0);
                        sp.edit().putString("ProhibitType", Config.aclType).commit();
                        //0 开放 1不开放 但是可跳转 2不开放且不可跳转 3 永久不可用(不再发起任何网络请求)
                        if(Config.aclType.equals("0")){
                            svAble.setVisibility(View.VISIBLE);
                            llUnable.setVisibility(View.GONE);
                        }else if(Config.aclType.equals("1")){
                            if(Config.isJump){
                                svAble.setVisibility(View.VISIBLE);
                                llUnable.setVisibility(View.GONE);
                            }else {
                                svAble.setVisibility(View.GONE);
                                llUnable.setVisibility(View.VISIBLE);
                            }
                        }else if(Config.aclType.equals("2")){
                            svAble.setVisibility(View.GONE);
                            llUnable.setVisibility(View.VISIBLE);
                        }else if(Config.aclType.equals("3")){
                            svAble.setVisibility(View.GONE);
                            llUnable.setVisibility(View.VISIBLE);
                        }else {
                            finish();
                        }
                    }
                });
    }
}
