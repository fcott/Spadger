package com.fcott.spadger.utils.imageUtils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.fcott.spadger.App;

public class ImageLifecycleManager {

    private RequestManager requestManager;

    public ImageLifecycleManager(Object context) {
        requestManager = getRequestManager(context);
    }

    public static RequestManager getRequestManager(@NonNull Object context) {
        if (context instanceof Activity) {
            RequestManager request = Glide.with((Activity) context);
            return request;
        } else if (context instanceof Context) {
            RequestManager request = Glide.with((Context) context);
            return request;
        } else if (context instanceof FragmentActivity) {
            RequestManager request = Glide.with((FragmentActivity) context);
            return request;
        } else if (context instanceof Fragment) {
            RequestManager request = Glide.with((Fragment) context);
            return request;
        }
        return null;
    }

    /**
     * @see android.content.ComponentCallbacks2#onTrimMemory(int)
     */
    public void onTrimMemory(int level) {
        try{
            if (null != requestManager) {
//                requestManager.onTrimMemory(level);
                Glide.get(App.getInstance()).onTrimMemory(level);

            }
        }catch (Exception e){}
    }

    /**
     * @see android.content.ComponentCallbacks2#onLowMemory()
     */
    public void onLowMemory() {
        try{
            if (null != requestManager) {
//                requestManager.onLowMemory();
                Glide.get(App.getInstance()).onLowMemory();
            }
        }catch (Exception e){}
    }

    /**
     * Returns true if loads for this {@link RequestManager} are currently paused.
     *
     * @see #pauseRequests()
     * @see #resumeRequests()
     */
    public boolean isPaused() {
        try{
            if (null != requestManager) {
                requestManager.isPaused();
            }
        }catch (Exception e){}
        return false;
    }

    /**
     * Cancels any in progress loads, but does not clear resources of completed loads.
     *
     * @see #isPaused()
     * @see #resumeRequests()
     */
    public void pauseRequests() {
        try{
            if (null != requestManager) {
                requestManager.pauseRequests();
            }
        }catch (Exception e){}
    }

    /**
     * Performs {@link #pauseRequests()} recursively for all managers that are contextually descendant
     * to this manager based on the Activity/Fragment hierarchy:
     * <p>
     * <ul>
     * <li>When pausing on an Activity all attached fragments will also get paused.
     * <li>When pausing on an attached Fragment all descendant fragments will also get paused.
     * <li>When pausing on a detached Fragment or the application context only the current RequestManager is paused.
     * </ul>
     * <p>
     * <p>Note, on pre-Jelly Bean MR1 calling pause on a Fragment will not cause child fragments to pause, in this
     * case either call pause on the Activity or use a support Fragment.
     */
    public void pauseRequestsRecursive() {
        try{
            if (null != requestManager) {
                requestManager.pauseRequestsRecursive();
            }
        }catch (Exception e){}
    }

    /**
     * Restarts any loads that have not yet completed.
     *
     * @see #isPaused()
     * @see #pauseRequests()
     */
    public void resumeRequests() {
        try{
            if (null != requestManager) {
                requestManager.resumeRequests();
            }
        }catch (Exception e){}
    }

    /**
     * Performs {@link #resumeRequests()} recursively for all managers that are contextually descendant
     * to this manager based on the Activity/Fragment hierarchy. The hierarchical semantics are identical as for
     * {@link #pauseRequestsRecursive()}.
     */
    public void resumeRequestsRecursive() {
        try{
            if (null != requestManager) {
                requestManager.resumeRequestsRecursive();
            }
        }catch (Exception e){}
    }

    /**
     * Lifecycle callback that registers for connectivity events (if the android.permission.ACCESS_NETWORK_STATE
     * permission is present) and restarts failed or paused requests.
     */
    public void onStart() {
        try{
            if (null != requestManager) {
                requestManager.onStart();
            }
        }catch (Exception e){}
    }

    /**
     * Lifecycle callback that unregisters for connectivity events (if the android.permission.ACCESS_NETWORK_STATE
     * permission is present) and pauses in progress loads.
     */
    public void onStop() {
        try{
            if (null != requestManager) {
                requestManager.onStop();
            }
        }catch (Exception e){}
    }

    /**
     * Lifecycle callback that cancels all in progress requests and clears and recycles resources for all completed
     * requests.
     */
    public void onDestroy() {
        try{
            if (null != requestManager) {
                requestManager.onDestroy();
            }
        }catch (Exception e){}
    }
}
