package com.fcott.spadger.utils.imageUtils;

import android.support.v7.widget.RecyclerView;

public class ImageRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    private ImageLifecycleManager imageLifecycleManager;

    /***
     *  由于用户的操作，屏幕产生惯性滑动，是否停止加载图片
     */
    private boolean isSettlingPause = true;

    public ImageRecyclerOnScrollListener(Object context) {
        imageLifecycleManager = new ImageLifecycleManager(context);
    }

    public ImageRecyclerOnScrollListener withSettlingPause(boolean settlingPause) {
        isSettlingPause = settlingPause;
        return this;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        switch (newState) {
            case RecyclerView.SCROLL_STATE_IDLE: // The RecyclerView is not currently scrolling.
                //当屏幕停止滚动，加载图片
                if (null != imageLifecycleManager) {
                    imageLifecycleManager.resumeRequests();
                    imageLifecycleManager.onStart();
                }
                break;
            case RecyclerView.SCROLL_STATE_DRAGGING: // The RecyclerView is currently being dragged by outside input such as user touch input.
                //当屏幕滚动且用户使用的触碰或手指还在屏幕上，加载图片
                if (null != imageLifecycleManager) {
                    imageLifecycleManager.resumeRequests();
                    imageLifecycleManager.onStart();
//                    imageLifecycleManager.pauseRequests();
//                    imageLifecycleManager.onStop();
                }
                break;
            case RecyclerView.SCROLL_STATE_SETTLING: // The RecyclerView is currently animating to a final position while not under outside control.
                //由于用户的操作，屏幕产生惯性滑动，停止加载图片
                if (null != imageLifecycleManager) {
                    if (isSettlingPause) {
                        imageLifecycleManager.pauseRequests();
                        imageLifecycleManager.onStop();
                    } else {
//                        imageLifecycleManager.pauseRequests();
//                        imageLifecycleManager.onStart();
                        imageLifecycleManager.pauseRequests();
                        imageLifecycleManager.onStop();
                    }
                }
                break;
        }
    }
}
