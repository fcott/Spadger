package com.fcott.spadger.utils.imageUtils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.fcott.spadger.utils.imageUtils.OptionConfiger.ISimpleImageOpConfig;
import com.fcott.spadger.utils.imageUtils.OptionConfiger.SimpleOpConfigerFactory;
import com.fcott.spadger.utils.imageUtils.bean.SimpleImageOpConfiger;

public class ImageTransfor implements ISimpleImageOpConfig {

    public static RequestBuilder loadBitmap(Object context, String url, ImageView imgView, SimpleImageOpConfiger ops) {
        return loadBitmap(context, url, imgView, ops, -1, -1);
    }

    public static RequestBuilder loadBitmap(Object context, String url, ImageView imgView, SimpleImageOpConfiger ops, int loadMode) {
        return loadBitmap(context, url, imgView, ops, -1, -1, loadMode);
    }

    public static RequestBuilder loadBitmap(Object context, String url, ImageView imgView, SimpleImageOpConfiger ops, int width, int heigt) {
        return loadBitmap(context, url, imgView, ops, width, heigt, 0);
    }

    /**
     * 加载bitmap
     *
     * @param context
     * @param url
     * @param imgView
     * @param ops
     * @param width
     * @param heigh
     * @param loadMode  0 bitmap 1 是 gif 2 lottie
     * @return
     */
    public static RequestBuilder loadBitmap(@NonNull Object context, String url, ImageView imgView, SimpleImageOpConfiger ops, int width, int heigh, int loadMode) {
        if (null == context)
            return null;
        if(null == ops){
            ops = SimpleOpConfigerFactory.getInstance().createMmHimageOps(imgOptionsEmpty);
        }
        ops.width = width;
        ops.heigt = heigh;

        try{
            if (context instanceof FragmentActivity) {
                FragmentActivity activity = (FragmentActivity) context;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && !activity.isDestroyed()) {
                    RequestManager request = Glide.with(activity);
                    return initOpsImage(url, ops, request, imgView, loadMode);
                } else if (!((Activity) context).isFinishing()) {
                    RequestManager request = Glide.with(activity);
                    return initOpsImage(url, ops, request, imgView, loadMode);
                }
            } else if (context instanceof Activity && !((Activity) context).isFinishing()) {
                Activity activity = (Activity) context;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && !activity.isDestroyed()) {
                    RequestManager request = Glide.with(activity);
                    return initOpsImage(url, ops, request, imgView, loadMode);
                } else if (!((Activity) context).isFinishing()) {
                    RequestManager request = Glide.with(activity);
                    return initOpsImage(url, ops, request, imgView, loadMode);
                }
            } else if (context instanceof Context) {
                RequestManager request = Glide.with((Context) context);
                return initOpsImage(url, ops, request, imgView, loadMode);
            } else if (context instanceof Fragment && null != ((Fragment) context).getActivity()) {
                RequestManager request = Glide.with((Fragment) context);
                return initOpsImage(url, ops, request, imgView, loadMode);
            }
        }catch (Exception e){}
        return null;
    }

    /**
     * 初始化图片加载参数
     *
     * @param url
     * @param ops
     * @param request
     * @param imgView
     * @param loadMode 1 是 gif 2 bitmap 3 lottie
     * @return
     */
    public static RequestBuilder initOpsImage(String url, SimpleImageOpConfiger ops, RequestManager request, ImageView imgView, int loadMode) {
        /*******构建requestBuilder*********/
        RequestBuilder builder = request.load(url);

        if (ops.thumbnail >= 0) {
            if (ops.thumbnail == 0) {
                builder.thumbnail();
            } else {
                builder.thumbnail(ops.thumbnail);
            }
        }
        if(loadMode == ImageLoadMode.IMAGE){
            /******设置参数********/
            builder.apply(buildOptions(ops, imgView, false));
            request.asBitmap();
            /*******设置动画*********/
            builder.transition(new DrawableTransitionOptions().crossFade());
            /*****************/
            if (null != imgView)
                builder.into(imgView);
        }
        return builder;
    }

    /**
     * 构建glide requestOptions
     *
     * @param ops 图片的参数
     * @return
     */
    public static RequestOptions buildOptions(SimpleImageOpConfiger ops, ImageView imageView, boolean isGif) {
        return new ImageRequestOptions(ops, imageView, isGif);
    }
//
//    public static class MmHTarget extends SimpleTarget<BitmapDrawable> {
//
//        private String url;
//        private SimpleImageLoadingListener mListener;
//
//        public MmHTarget(String url, SimpleImageLoadingListener mListener) {
//            this.url = url;
//            this.mListener = mListener;
//        }
//
//        @Override
//        public void onLoadFailed(@Nullable Drawable errorDrawable) {
//            super.onLoadFailed(errorDrawable);
//            mListener.onLoadingFailed(url);
//        }
//
//        @Override
//        public void onResourceReady(BitmapDrawable resource, Transition<? super BitmapDrawable> transition) {
//            mListener.onLoadingComplete(url, resource.getBitmap());
//        }
//    }

    public interface ImageLoadMode{
        int IMAGE = 0;
        int GIF = 1;
        int LOTTIE = 2;
    }
}
