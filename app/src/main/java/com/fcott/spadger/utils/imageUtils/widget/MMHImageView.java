package com.fcott.spadger.utils.imageUtils.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

public class MMHImageView extends android.support.v7.widget.AppCompatImageView {

    private OnInvalidateDrawable onInvalidateDrawable;

    private onAttachedWindowChagne onAttachedWindowChagne;

    private onSaveInstanceStateChange onSaveInstanceStateChange;

    private MMHImageView.onSaveInstanceStateChange.OnRestoreStateAgent onRestoreStateAgent;

    public MMHImageView(Context context) {
        super(context);
    }

    public MMHImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MMHImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void invalidateDrawable(@NonNull Drawable dr) {
        if (null != onInvalidateDrawable && !onInvalidateDrawable.invalidateDrawable()) {
            return;
        }
        super.invalidateDrawable(dr);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        if (null != onSaveInstanceStateChange) {
            Parcelable newSatate = onSaveInstanceStateChange.onSaveInstanceState(superState);
            if (null != newSatate) {
                return newSatate;
            }
        }
        return superState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (null != onSaveInstanceStateChange) {
            if (null == onRestoreStateAgent) {
                onRestoreStateAgent = new onSaveInstanceStateChange.OnRestoreStateAgent() {
                    @Override
                    public void onRestoreInstanceState(Parcelable state) {
                        MMHImageView.super.onRestoreInstanceState(state);
                    }
                };
            }
            onSaveInstanceStateChange.onRestoreInstanceState(state, onRestoreStateAgent);
        } else {
            super.onRestoreInstanceState(state);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        if (null != onAttachedWindowChagne && !onAttachedWindowChagne.onAttachedToWindow()) {
            return;
        }
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        if (null != onAttachedWindowChagne && !onAttachedWindowChagne.onDetachedFromWindow()) {
            return;
        }
        super.onDetachedFromWindow();
    }

    public void setOnInvalidateDrawable(OnInvalidateDrawable onInvalidateDrawable) {
        this.onInvalidateDrawable = onInvalidateDrawable;
    }

    public interface OnInvalidateDrawable {
        boolean invalidateDrawable();
    }

    public void setOnAttachedWindowChagne(MMHImageView.onAttachedWindowChagne onAttachedWindowChagne) {
        this.onAttachedWindowChagne = onAttachedWindowChagne;
    }

    public interface onAttachedWindowChagne {
        boolean onAttachedToWindow();

        boolean onDetachedFromWindow();
    }

    public void setOnSaveInstanceStateChange(MMHImageView.onSaveInstanceStateChange onSaveInstanceStateChange) {
        this.onSaveInstanceStateChange = onSaveInstanceStateChange;
    }

    public interface onSaveInstanceStateChange {

        Parcelable onSaveInstanceState(Parcelable superState);

        void onRestoreInstanceState(Parcelable state, OnRestoreStateAgent onRestoreStateAgent);

        interface OnRestoreStateAgent {
            void onRestoreInstanceState(Parcelable state);
        }
    }
}
