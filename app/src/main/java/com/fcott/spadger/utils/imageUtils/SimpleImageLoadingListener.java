package com.fcott.spadger.utils.imageUtils;

import android.graphics.Bitmap;

public interface SimpleImageLoadingListener {
	void onLoadingComplete(String imageUri, Bitmap loadedImage);

	void onLoadingFailed(String imageUri);
}
