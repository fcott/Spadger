package com.fcott.spadger.utils.imageUtils.moitor;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.fcott.spadger.utils.imageUtils.SimpleImageLoadingListener;

public class MmhImageLoadMonitor extends SimpleTarget {

    private String url;
    private SimpleImageLoadingListener mListener;

    public MmhImageLoadMonitor(String url, SimpleImageLoadingListener mListener) {
        this.url = url;
        this.mListener = mListener;
    }

    @Override
    public void onLoadFailed(@Nullable Drawable errorDrawable) {
        super.onLoadFailed(errorDrawable);
        if (!TextUtils.isEmpty(url) && null != mListener)
            mListener.onLoadingFailed(url);
    }

    @Override
    public void onResourceReady(Object resource, Transition transition) {
        if (null != resource && resource instanceof BitmapDrawable) {
            if (!TextUtils.isEmpty(url) && null != mListener && null != resource && null != ((BitmapDrawable) resource).getBitmap())
                mListener.onLoadingComplete(url, ((BitmapDrawable) resource).getBitmap());
            else if (null != mListener) {
                mListener.onLoadingFailed(url);
            }
        }else if (null != resource && resource instanceof GifDrawable) {
            if (!TextUtils.isEmpty(url) && null != mListener && null != resource && null != ((GifDrawable) resource).getFirstFrame())
                mListener.onLoadingComplete(url, ((GifDrawable) resource).getFirstFrame());
            else if (null != mListener) {
                mListener.onLoadingFailed(url);
            }
        }else {
            if (null != mListener) {
                mListener.onLoadingFailed(url);
            }
        }
    }
}
