package com.fcott.spadger.utils.imageUtils;

import android.content.Context;
import android.text.TextUtils;

import java.io.File;


public class ImageUtils {

    public static String getCompressImgUrl(String url, int width, int heigh, int quality) {
        return getCompressImgUrl(url, width, heigh, 0, quality);
    }

    public static String getCompressImgUrl(String url, int quality) {
        return String.format("%1$s?x-oss-process=image/quality,q_%4$d", url, quality);
    }

    public static String getCompressImgUrl(String url, int width, int heigh, int angle, int quality) {
        if (width == 0 || heigh == 0) {
            return url;
        }
        if (!OssUpImageConfiger.isContain(url))
            return url;
        if (TextUtils.isEmpty(url))
            return url;
        url = OssUpImageConfiger.formatUrl(url);
        if (url.contains("x-oss-process=image"))
            return url;
        if(angle > 0){
            return String.format("%1$s?x-oss-process=image/rounded-corners,r_"+ angle +"/resize,w_%2$d,h_%3$d/quality,q_%4$d", url, width, heigh,
                    quality);
        }
        return String.format("%1$s?x-oss-process=image/resize,w_%2$d,h_%3$d/quality,q_%4$d", url, width, heigh,
                quality);
    }
}
