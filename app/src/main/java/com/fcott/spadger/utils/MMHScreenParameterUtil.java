package com.fcott.spadger.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.fcott.spadger.App;

public class MMHScreenParameterUtil {
    private static int screenWidth;
    private static int screenHeight;
    private static float scaledDensity;
    private static float density;
    private static int densityDpi;

    /**
     * 获取屏幕宽度
     */
    public static int getScreenWidth() {
        if(screenWidth != 0)
            return screenWidth;
        initScreenParameter();
        return screenWidth;
    }

    /**
     * 获取屏幕高度
     */
    public static int getScreenHeight() {
        if(screenHeight != 0)
            return screenHeight;
        initScreenParameter();
        return screenHeight;
    }

    public static float getScreenScaledDensity(){
        if(scaledDensity != 0)
            return scaledDensity;
        initScreenParameter();
        return scaledDensity;
    }

    public static float getScreenDensity(){
        if(density != 0)
            return density;
        initScreenParameter();
        return density;
    }

    public static float getScreenDensityDpi(){
        if(densityDpi != 0)
            return densityDpi;
        initScreenParameter();
        return densityDpi;
    }

    /**
     * 将dip或dp值转换为px值，保证尺寸不变
     * @param dipValue
     * @return
     */
    public static int dip2px(float dipValue){
        final float scale = MMHScreenParameterUtil.getScreenDensity();
        return (int) (dipValue * scale + 0.5f);
    }

    /***
     * 兼容老方法
     * @param mContext
     * @param dipValue
     * @return
     */
    @Deprecated
    public static int dip2px(Context mContext, float dipValue){
        return dip2px(dipValue);
    }

    /**
     * 将px值转换为dip或dp值，保证尺寸大小不变
     * @param pxValue
     * @return
     */
    public static int px2dp( float pxValue){
        final float scale = MMHScreenParameterUtil.getScreenDensity();
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将px值转换为sp值，保证文字大小不变
     * @param pxValue
     * @return
     */
    public static int px2sp( float pxValue) {
        final float fontScale = MMHScreenParameterUtil.getScreenScaledDensity();
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     * @param spValue
     * @return
     */
    public static int sp2px( float spValue) {
        final float fontScale = MMHScreenParameterUtil.getScreenScaledDensity();
        return (int) (spValue * fontScale + 0.5f);
    }

    private static void initScreenParameter(){
        Context context = App.getInstance();
        if(null == context) return;
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        screenWidth = dm.widthPixels;
        screenHeight = dm.heightPixels;
        scaledDensity = dm.scaledDensity;
        density = dm.density;
        densityDpi = dm.densityDpi;
        if(screenWidth == 0 || screenHeight == 0){
            Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            screenHeight = display.getHeight();
            screenWidth = display.getWidth();
        }
    }
}
