package com.fcott.spadger.utils.imageUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.fcott.spadger.utils.LogUtil;
import com.fcott.spadger.utils.imageUtils.OptionConfiger.ISimpleImageOpConfig;
import com.fcott.spadger.utils.imageUtils.OptionConfiger.SimpleOpConfigerFactory;
import com.fcott.spadger.utils.imageUtils.bean.SimpleImageOpConfiger;
import com.fcott.spadger.utils.imageUtils.moitor.MmhImageLoadMonitor;

import java.util.concurrent.ExecutionException;


public class ImageCacheManager implements ISimpleImageOpConfig {

    public static SimpleImageOpConfiger getRadiusConfiger(int radius){
        SimpleImageOpConfiger simpleImageOpConfiger = new SimpleImageOpConfiger();
        simpleImageOpConfiger.radius = radius;
        return simpleImageOpConfiger;
    }
    public static SimpleImageOpConfiger getRadiusConfigerByCenterCrop(int radius) {
        SimpleImageOpConfiger simpleImageOpConfiger = new SimpleImageOpConfiger();
        simpleImageOpConfiger.radius = radius;
        simpleImageOpConfiger.needCenterCrop = true;
        return simpleImageOpConfiger;
    }

    public static void showImage(Object instace, String url, ImageView imgView, SimpleImageOpConfiger ops) {
        if (isLoadLottieImage(url)) {
            showLottieImage(instace, url, imgView, ops);
            return;
        }

        // 判断是否是gif 如果是 就加载gif
        if (isLoadGifImage(url)) {
            showGifImage(instace, url, imgView, ops);
            return;
        }
        url = SimpleImageUrlOp.formatUrl(url);
        ImageTransfor.loadBitmap(instace, url, imgView, ops);
    }

    public static void showImage(Object instace, String url, ImageView imgView, int... configType) {
        if (isLoadLottieImage(url)) {
            showLottieImage(instace, url, imgView, configType);
            return;
        }

        // 判断是否是gif 如果是 就加载gif
        if (isLoadGifImage(url)) {
            showGifImage(instace, url, imgView, configType);
            return;
        }
        url = SimpleImageUrlOp.formatUrl(url);
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        ImageTransfor.loadBitmap(instace, url, imgView, ops);
    }

    public static void showImage(Object instace, String url, ImageView imgView, int width, int heigh, SimpleImageOpConfiger ops) {
        if (isLoadLottieImage(url)) {
            showLottieImage(instace, url, imgView, ops);
            return;
        }

        String imageUrl = ImageUtils.getCompressImgUrl(url, width, heigh, 70);

        // 判断是否是gif 如果是 就加载gif
        if (isLoadGifImage(url)) {
            showGifImage(instace, imageUrl, width, heigh, imgView, ops);
            return;
        }
        imageUrl = SimpleImageUrlOp.formatUrl(imageUrl);
        ImageTransfor.loadBitmap(instace, imageUrl, imgView, ops, width, heigh);
    }

    public static void showImage(Object instace, String url, int width, int heigh, ImageView imgView, int... configType) {
        if (isLoadLottieImage(url)) {
            showLottieImage(instace, url, imgView, configType);
            return;
        }

        String imageUrl = ImageUtils.getCompressImgUrl(url, width, heigh, 70);

        // 判断是否是gif 如果是 就加载gif
        if (isLoadGifImage(url)) {
            showGifImage(instace, imageUrl, width, heigh, imgView, configType);
            return;
        }
        imageUrl = SimpleImageUrlOp.formatUrl(imageUrl);
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        ImageTransfor.loadBitmap(instace, imageUrl, imgView, ops, width, heigh);
    }

    public static void showImageBySDcard(Object instace, String url, ImageView imgView, int... configType) {
        if (isLoadLottieImage(url)) {
            showLottieImage(instace, url, imgView, configType);
            return;
        }

        // 判断是否是gif 如果是 就加载gif
        if (isLoadGifImage(url)) {
            showGifImage(instace, url, imgView, configType);
            return;
        }
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        ImageTransfor.loadBitmap(instace, url, imgView, ops);
    }

    public static void loadImage(Object instace, String url, ImageView imgView, SimpleImageLoadingListener mListener, int... configType) {
        url = SimpleImageUrlOp.formatUrl(url);
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, imgView, ops);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void loadImageSd(Object instace, String url, ImageView imgView, SimpleImageLoadingListener mListener, int... configType) {
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, imgView, ops);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void showImageBySDcard(Object instace, String url, ImageView imgView, SimpleImageOpConfiger ops) {
        if (isLoadLottieImage(url)) {
            showLottieImage(instace, url, imgView, ops);
            return;
        }

        // 判断是否是gif 如果是 就加载gif
        if (isLoadGifImage(url)) {
            showGifImage(instace, url, imgView, ops);
            return;
        }
        ImageTransfor.loadBitmap(instace, url, imgView, ops);
    }

    public static void showImageBySDcard(Object instace, String url, ImageView imgView, int width, int heigh, SimpleImageOpConfiger ops) {
        if (isLoadLottieImage(url)) {
            showLottieImage(instace, url, imgView, ops);
            return;
        }

        // 判断是否是gif 如果是 就加载gif
        if (isLoadGifImage(url)) {
            showGifImage(instace, url, imgView, ops);
            return;
        }
        ImageTransfor.loadBitmap(instace, url, imgView, ops, width, heigh);
    }

    public static void showImageBySDcard(Object instace, String url, ImageView imgView) {// 判断是否是gif 如果是 就加载gif
        if (isLoadLottieImage(url)) {
            showLottieImage(instace, url, imgView);
            return;
        }

        // 判断是否是gif 如果是 就加载gif
        if (isLoadGifImage(url)) {
            showGifImage(instace, url, imgView);
            return;
        }
        ImageTransfor.loadBitmap(instace, url, imgView, SimpleOpConfigerFactory.getInstance().createMmHimageOps(imgOptionsEmpty));
    }

    public static void loadImage(Object instace, String url, SimpleImageOpConfiger ops, SimpleImageLoadingListener mListener) {
        url = SimpleImageUrlOp.formatUrl(url);
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, ops);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void loadImage(Object instace, String url, int width, int heigh, SimpleImageOpConfiger ops, SimpleImageLoadingListener mListener) {
        url = ImageUtils.getCompressImgUrl(url, width, heigh, 70);
        url = SimpleImageUrlOp.formatUrl(url);
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, ops, width, heigh);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void loadImage(Object instace, String url, SimpleImageLoadingListener mListener, int... configType) {
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);

        url = SimpleImageUrlOp.formatUrl(url);
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, ops);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void loadImage(Object instace, String url, int width, int heigh, SimpleImageLoadingListener mListener, int... configType) {
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        url = ImageUtils.getCompressImgUrl(url, width, heigh, 70);
        url = SimpleImageUrlOp.formatUrl(url);
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, ops);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void loadImage(Object instace, String url, int mQ, SimpleImageLoadingListener mListener, int... configType) {
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        url = ImageUtils.getCompressImgUrl(url,mQ);
        url = SimpleImageUrlOp.formatUrl(url);
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, ops);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void loadImage(Object instace, String url, SimpleImageLoadingListener mListener) {
        url = SimpleImageUrlOp.formatUrl(url);
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, SimpleOpConfigerFactory.getInstance().createMmHimageOps(imgOptionsEmpty));
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

//    public static void loadImage(Object instace, String url, ImageView imageView, GifImageLoadingListener mListener) {
//        url = SimpleImageUrlOp.formatUrl(url);
//        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, SimpleOpConfigerFactory.getInstance().createMmHimageOps(imgOptionsEmpty));
//        request.into(new MmhGifImageLoadMonitor(imageView, url, mListener));
//    }

    public static void loadImageSd(Object instace, String url, SimpleImageOpConfiger ops, SimpleImageLoadingListener mListener) {
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, ops);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void loadImageSd(Object instace, String url, SimpleImageLoadingListener mListener) {
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, SimpleOpConfigerFactory.getInstance().createMmHimageOps(imgOptionsEmpty));
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void loadGifSd(Object instace, String url, SimpleImageLoadingListener mListener) {
        RequestBuilder request = ImageTransfor.loadBitmap(instace, url, null, SimpleOpConfigerFactory.getInstance().createMmHimageOps(imgOptionsEmpty), ImageTransfor.ImageLoadMode.GIF);
        request.into(new MmhImageLoadMonitor(url, mListener));
    }

    public static void showGifImage(Object instace, String url, ImageView imgView, SimpleImageOpConfiger ops) {
        ImageTransfor.loadBitmap(instace, url, imgView, ops, ImageTransfor.ImageLoadMode.GIF);
    }

    public static void showGifImage(Object instace, String url, int width, int heigh, ImageView imgView, SimpleImageOpConfiger ops) {
        ImageTransfor.loadBitmap(instace, url, imgView, ops, width, heigh, ImageTransfor.ImageLoadMode.GIF);
    }

    public static void showGifImage(Object instace, String url, int width, int heigh, ImageView imgView, int... configType) {
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        ImageTransfor.loadBitmap(instace, url, imgView, ops, width, heigh, ImageTransfor.ImageLoadMode.GIF);
    }

    public static void showGifImage(Object instace, String url, ImageView imgView, int... configType) {
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        ImageTransfor.loadBitmap(instace, url, imgView, ops, ImageTransfor.ImageLoadMode.GIF);
    }

    public static boolean isLoadGifImage(String url) {
        return !TextUtils.isEmpty(url) && url.endsWith(".gif");
    }

    public static boolean isLoadLottieImage(String url) {
        return !TextUtils.isEmpty(url) && url.endsWith(".json");
    }

    public static void showLottieImage(Object instace, String url, ImageView imgView, SimpleImageOpConfiger ops) {
        ImageTransfor.loadBitmap(instace, url, imgView, ops, ImageTransfor.ImageLoadMode.LOTTIE);
    }

    public static void showLottieImage(Object instace, String url, ImageView imgView, int... configType) {
        SimpleImageOpConfiger ops = getSimpleImageOpConfiger(configType);
        ImageTransfor.loadBitmap(instace, url, imgView, ops, ImageTransfor.ImageLoadMode.LOTTIE);
    }

    private static SimpleImageOpConfiger getSimpleImageOpConfiger(int[] configType) {
        SimpleImageOpConfiger ops = null;
        if (configType.length == 1) {
            ops = SimpleOpConfigerFactory.getInstance().createMmHimageOps(configType[0]);
        } else if (configType.length == 2) {
            ops = SimpleOpConfigerFactory.getInstance().createMmHimageOps(configType[0], configType[1]);
        } else {
            ops = SimpleOpConfigerFactory.getInstance().createMmHimageOps(imgOptionsEmpty);
        }
        return ops;
    }

    /***
     * 同步 根据宽高 获取图片
     * @param mContext
     * @param url
     * @param width
     * @param heigh
     * @return
     */
    public static Bitmap loadImageSync(Context mContext, String url, int width, int heigh) {
        try {
            url = SimpleImageUrlOp.formatUrl(url);
            Bitmap mBitmap = Glide.with(mContext).asBitmap().load(url).apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false)).submit(width, heigh).get();
            return mBitmap;
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        }
        return null;
    }

    /***
     * 同步 获取图片
     * @param mContext
     * @param url
     * @return
     */
    public static Bitmap loadImageSync(Context mContext, String url) {
        try {
            url = SimpleImageUrlOp.formatUrl(url);
            Bitmap mBitmap = Glide.with(mContext).asBitmap().load(url).apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false)).submit().get();
            return mBitmap;
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        }
        return null;
    }

    public static Bitmap loadImageSyncUrl(Context mContext, String url, int width, int height) {
        try {
            url = ImageUtils.getCompressImgUrl(url, width, height, 70);
            url = SimpleImageUrlOp.formatUrl(url);
            Bitmap mBitmap = Glide.with(mContext).asBitmap().load(url).apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false)).submit(width, height).get();
            return mBitmap;
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        }
        return null;
    }

    public static GifDrawable loadGifSync(Context mContext, String url) {
        try {
            url = SimpleImageUrlOp.formatUrl(url);
            GifDrawable mBitmap = Glide.with(mContext).asGif().load(url).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false)).submit().get();
            return mBitmap;
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        }
        return null;
    }

    public static GifDrawable loadGifSync(Context mContext, String url, int width, int heigh) {
        try {
            url = SimpleImageUrlOp.formatUrl(url);
            GifDrawable mBitmap = Glide.with(mContext).asGif().load(url).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false)).submit(width, heigh).get();
            return mBitmap;
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        }
        return null;
    }

    /***
     * 同步 获取 sdCard 图片
     * @param mContext
     * @param url
     * @param width
     * @param heigh
     * @return
     */
    public static Bitmap loadImageSyncBySDCard(Context mContext, String url, int width, int heigh) {
        try {
            Bitmap mBitmap = Glide.with(mContext).asBitmap().load(url).apply(new RequestOptions().centerCrop()).submit(width, heigh).get();
            return mBitmap;
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        }
        return null;
    }

    public static Bitmap loadImageSyncBySDCard(Context mContext, String url) {
        try {
            Bitmap mBitmap = Glide.with(mContext).asBitmap().load(url).apply(new RequestOptions().centerCrop()).submit().get();
            return mBitmap;
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        }
        return null;
    }

    /**
     * 清除内存缓存
     *
     * @param context
     */
    @NonNull
    public static void clearMemoryCache(Context context) {
        if (null != context) Glide.get(context).clearMemory();
    }

    /**
     * 清除磁盘缓存
     *
     * @param context
     */
    @NonNull
    public static void clearDiskCache(Context context) {
        if (null != context) Glide.get(context).clearDiskCache();
    }

    @NonNull
    public static void resumeRequests(Context context) {
        try {
            if (null != context) Glide.with(context).resumeRequests();
        } catch (Exception e) {

        }
    }

    @NonNull
    public static void pauseRequests(Context context) {
        try {
            if (null != context) Glide.with(context).pauseRequests();
        } catch (Exception e) {

        }
    }

    @NonNull
    public static void onDestroy(Context context) {
        try {
            if (null != context) Glide.with(context).onDestroy();
        } catch (Exception e) {

        }
    }

    @NonNull
    public static void onTrimMemory(Context context, int level) {
        try {
            if (null != context) Glide.get(context).onTrimMemory(level);
        } catch (Exception e) {

        }
    }

    @NonNull
    public static void onLowMemory(Context context) {
        try {
            if (null != context) Glide.get(context).onLowMemory();
        } catch (Exception e) {

        }
    }
}
