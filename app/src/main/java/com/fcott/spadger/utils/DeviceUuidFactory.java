package com.fcott.spadger.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;

/**
 * 获取手机唯一标识
 */
public class DeviceUuidFactory {

    public static String getDeviceIdByUiss(Context context) {
        try {
            //The Android ID
            @SuppressLint("HardwareIds") String m_szAndroidID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

            return m_szAndroidID ;
        } catch (Exception e) {
            return "unknowedevice";
        }

    }

}
