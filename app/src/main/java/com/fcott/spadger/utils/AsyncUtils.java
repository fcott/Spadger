package com.fcott.spadger.utils;


import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class AsyncUtils {

    public static void asyncAsResult(final AsyncObjecyerResult asyncObjecyer){
        Observable.just(asyncObjecyer).subscribeOn(Schedulers.newThread()).map(new Func1<AsyncObjecyerResult, Object>() {
            @Override
            public Object call(AsyncObjecyerResult asyncObjecyer) {
                Object result = asyncObjecyer.doAsyncAction();
                return result;
            }
        }).observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                asyncObjecyer.asyncResult(o);
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                asyncObjecyer.asyncResult(null);
            }
        });
    }

    public static void async(final AsyncObjecyer asyncObjecyer){
        Observable.just(asyncObjecyer).subscribeOn(Schedulers.newThread()).map(new Func1<AsyncObjecyer, Object>() {
            @Override
            public Object call(AsyncObjecyer asyncObjecyer) {
                asyncObjecyer.doAsyncAction();
                return true;
            }
        }).observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {}
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }

    public interface AsyncObjecyer{
        void doAsyncAction();
    }

    public interface AsyncObjecyerResult{
        Object doAsyncAction();
        void asyncResult(Object object);
    }
}
