package com.fcott.spadger.utils.imageUtils.OptionConfiger;

public interface ISimpleImageOpConfig {

    /***
     * 标准小图
     */
    int imgOptionsSmall = 1;

    /***
     * 标准中图
     */
    int imgOptionsMiddle = 2;

    /***
     * 标准大图
     */
    int imgOptionsBig = 3;

    /***
     * 群组 默认类型
     */
    int imgOptionsGroup = 4;

    /***
     * 用户头像
     */
    int imgOptionsMam = 5;

    /***
     * 没有默认 显示图片
     */
    int imgOptionsEmpty = 6;

    /***
     * 自定义 类型
     */
    int imgOptionsCustom = 7;

    /***
     * 不设置
     */
    int noId = -1;

    /***
     * 默认 是否 缓存到内存中
     */
    boolean defaultCacheInMemory = true;

    /***
     * 默认 是否缓存到sd卡中
     */
    boolean defaultCacheOnDisk = true;

    /***
     * 代表 圆形角度
     */
    int circleRadius = 3600;
}
