package com.fcott.spadger.utils.imageUtils;

import android.content.Context;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

@GlideModule
public class SimpleGlideModule extends AppGlideModule {
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        RequestOptions options = new RequestOptions();
        options.format(DecodeFormat.PREFER_ARGB_8888);
        builder.setDefaultRequestOptions(options);
        /**这个地方 给默认缓存大小 增加了 20% 因为使用默认的大小界面 会有卡顿现象**/
        // 缓存大小计算的类
        MemorySizeCalculator calculator = new MemorySizeCalculator.Builder(context).build();
        // 默认缓存大小
        int defaultMemoryCacheSize = calculator.getMemoryCacheSize();
        int defaultBitmapPoolSize = calculator.getBitmapPoolSize();
        // 设置增大 20%
        int customMemoryCacheSize = (int) (1 * defaultMemoryCacheSize);
        int customBitmapPoolSize = (int) (1.2 * defaultBitmapPoolSize);
        builder.setMemoryCache(new LruResourceCache(customMemoryCacheSize));
        builder.setBitmapPool(new LruBitmapPool(customBitmapPoolSize));
        /**这个地方 给默认缓存大小 增加了 20% 因为使用默认的大小界面 会有卡顿现象**/
        int cacheSize100MegaBytes = 1024 * 1024 * 100;
        //设置磁盘缓存到应用的内部目录，并且设置了最大的大小为 100M
        builder.setDiskCache(
                new InternalCacheDiskCacheFactory(context, cacheSize100MegaBytes)
        );
        //开启没有引用的图片的回收。看看好用不
        builder.setIsActiveResourceRetentionAllowed(true);
    }

    /**
     * 清单解析的开启
     * 这里不开启，避免添加相同的modules两次
     *
     * @return
     */
    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }
}