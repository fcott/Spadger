package com.fcott.spadger.utils.imageUtils;

import android.text.TextUtils;

public class SimpleImageUrlOp {

	/**
	 * qq头像地址域名
	 */
	public static String qqHost = "q.qlogo.cn";

	public static boolean isContain(String url){
		if(TextUtils.isEmpty(url)){
			return false;
		}

		/***这里可以做一些 url 的判断 是否 符合 某些条件***/
		if(OssUpImageConfiger.isContain(url))
			return true;

		return false;
	}

	public static String formatUrl(String url) {
		url = OssUpImageConfiger.formatUrl(url);
		if (SimpleImageUrlOp.isContain(url)) {
			if (url.contains("x-oss-process=image")) {
				if(!url.contains("/format,webp")){
					url = url + "/format,webp";
				}
			} else if (url.contains(".gif")){
				return url;
			}else {
				if(!url.contains("?x-oss-process=image/format,webp")){
					url = url + "?x-oss-process=image/format,webp";
				}
			}
		}else{

		}
		return url;
	}
}