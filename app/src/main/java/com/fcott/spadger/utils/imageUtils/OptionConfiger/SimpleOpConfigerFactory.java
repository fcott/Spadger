package com.fcott.spadger.utils.imageUtils.OptionConfiger;

/***
 * @设计思路 这个地方主要就是 创建一个 配置的单例队列，通过这个队列来获取用户想要的配置信息
 */


import com.fcott.spadger.R;
import com.fcott.spadger.utils.imageUtils.bean.SimpleImageOpConfiger;

import java.util.HashMap;
import java.util.Map;

public class SimpleOpConfigerFactory implements ISimpleImageOpConfig {

    private volatile static SimpleOpConfigerFactory singleClazz;
    private Map<Integer, SimpleImageOpConfiger> mOps = new HashMap<>();

    private SimpleOpConfigerFactory() {
    }

    public static SimpleOpConfigerFactory getInstance() {
        if (singleClazz == null) {
            synchronized (SimpleOpConfigerFactory.class) {
                if (singleClazz == null) {
                    singleClazz = new SimpleOpConfigerFactory();
                }
            }
        }
        return singleClazz;
    }

    public SimpleImageOpConfiger createMmHimageOps(int type) {
        if (mOps.containsKey(type)) {
            return mOps.get(type);
        } else {
            SimpleImageOpConfiger ops = getMmHimageOps(type, noId, noId, defaultCacheInMemory, defaultCacheOnDisk);
            mOps.put(type, ops);
            return ops;
        }
    }

    public SimpleImageOpConfiger createMmHimageOps(int type, int pic) {
        return createMmHimageOps(type, pic, pic, defaultCacheInMemory, defaultCacheOnDisk);
    }

    public SimpleImageOpConfiger createMmHimageOps(int type, int errorPic, int loadingPic) {
        return createMmHimageOps(type, errorPic, loadingPic, defaultCacheInMemory, defaultCacheOnDisk);
    }

    public SimpleImageOpConfiger createMmHimageOps(int type, int pic, boolean isCacheInMemory, boolean isCacheOnDisk) {
        return createMmHimageOps(type, pic, pic, isCacheInMemory, isCacheOnDisk);
    }

    public SimpleImageOpConfiger createMmHimageOps(int type, int errorPic, int loadingPic, boolean isCacheInMemory, boolean isCacheOnDisk) {
        if (mOps.containsKey(type)) {
            SimpleImageOpConfiger ops = mOps.get(type);
            ops.errorPic = errorPic;
            ops.loadingPic = loadingPic;
            ops.isCacheInMemory = isCacheInMemory;
            ops.isCacheOnDisk = isCacheOnDisk;
            return ops;
        } else {
            SimpleImageOpConfiger ops = getMmHimageOps(type, errorPic, loadingPic, isCacheInMemory, isCacheOnDisk);
            mOps.put(type, ops);
            return ops;
        }
    }

    private SimpleImageOpConfiger getMmHimageOps(int type, int errorPic, int loadingPic, boolean isCacheInMemory, boolean isCacheOnDisk) {
        SimpleImageOpConfiger ops = new SimpleImageOpConfiger();
        switch (type) {
            case imgOptionsSmall:
                ops.errorPic = R.color.C1;
                ops.loadingPic = noId;
                ops.isCacheInMemory = isCacheInMemory;
                ops.isCacheOnDisk = isCacheOnDisk;
                break;
            case imgOptionsMiddle:
                ops.errorPic = R.color.C1;
                ops.loadingPic = noId;
                ops.isCacheInMemory = isCacheInMemory;
                ops.isCacheOnDisk = isCacheOnDisk;
                break;
            case imgOptionsBig:
                ops.errorPic = R.color.C1;
                ops.loadingPic = noId;
                ops.isCacheInMemory = isCacheInMemory;
                ops.isCacheOnDisk = isCacheOnDisk;
                break;
            case imgOptionsGroup:
                ops.errorPic = R.color.C1;
                ops.loadingPic = noId;
                ops.isCacheInMemory = isCacheInMemory;
                ops.isCacheOnDisk = isCacheOnDisk;
                break;
            case imgOptionsEmpty:
                ops.errorPic = noId;
                ops.loadingPic = noId;
                ops.isCacheInMemory = isCacheInMemory;
                ops.isCacheOnDisk = isCacheOnDisk;
                break;
            case imgOptionsMam:
                ops.errorPic = R.color.C1;
                ops.loadingPic = noId;
                ops.isCacheInMemory = isCacheInMemory;
                ops.isCacheOnDisk = isCacheOnDisk;
                break;
            case imgOptionsCustom:
                ops.errorPic = errorPic;
                ops.loadingPic = noId;
                ops.isCacheInMemory = isCacheInMemory;
                ops.isCacheOnDisk = isCacheOnDisk;
                break;

            default:
                ops.errorPic = R.color.C1;
                ops.loadingPic = noId;
                ops.isCacheInMemory = isCacheInMemory;
                ops.isCacheOnDisk = isCacheOnDisk;
                break;
        }

        return ops;
    }
}
