package com.fcott.spadger.utils.imageUtils;

import android.widget.ImageView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.fcott.spadger.utils.imageUtils.OptionConfiger.ISimpleImageOpConfig;
import com.fcott.spadger.utils.imageUtils.bean.SimpleImageOpConfiger;
import com.fcott.spadger.utils.imageUtils.transform.GlideCircleTransform;
import com.fcott.spadger.utils.imageUtils.transform.GlideRoundTransform;

public class ImageRequestOptions extends RequestOptions implements ISimpleImageOpConfig {
    /**
     * 图片的参数
     */
    private SimpleImageOpConfiger ops;
    ImageView imageView;
    private boolean isGif;

    public ImageRequestOptions() {
    }

    public ImageRequestOptions(SimpleImageOpConfiger ops, ImageView imageView, boolean isGif) {
        this.ops = ops;
        this.imageView = imageView;
        this.isGif = isGif;
        initOptions();
    }

    private void initOptions() {
        /**********设置此属性 解决变绿问题  不设置会有问题********/
        if (ops.isCencerImage)
            centerCrop();
        /*****设置展位图和错误的图片*****/
        if (ops != null && null != ops.loadingDrawable) {
            placeholder(ops.loadingDrawable);
        } else if (ops != null && ops.loadingPic != noId) {
            placeholder(ops.loadingPic);
        }
        if (ops != null && null != ops.errorDrawable) {
            error(ops.errorDrawable);
            fallback(ops.errorDrawable);
        } else if (ops != null && ops.errorPic != noId) {
            error(ops.errorPic);
            fallback(ops.errorPic);
        }
        /****设置宽度和高度***/
        if (ops != null && ops.width != -1 && ops.heigt != -1 && ops.width > 0 && ops.heigt > 0) {
            override(ops.width, ops.heigt);
        }

        /********设置圆角**********/
        if (null != ops && ops.radius != -1 && null != imageView) {
            if (ops.radius == ISimpleImageOpConfig.circleRadius) {
                transform(new GlideCircleTransform());
            } else if (ops.needCenterCrop) {
                transform(new MultiTransformation(new CenterCrop(), new GlideRoundTransform(ops.radius)));
            } else {
                transform(new GlideRoundTransform(ops.radius));
            }
        }
        if (ops != null) {
            // 是否 使用 sd 卡缓存 使用 DiskCacheStrategy.SOURCE 解决过渡压缩导致的 图片变绿问题
            diskCacheStrategy(ops.isCacheOnDisk ? (isGif ? DiskCacheStrategy.ALL : DiskCacheStrategy.RESOURCE) : DiskCacheStrategy.NONE);
            // 是否 跳过 缓存到内存中
            skipMemoryCache(!ops.isCacheInMemory);
        }
    }
}
