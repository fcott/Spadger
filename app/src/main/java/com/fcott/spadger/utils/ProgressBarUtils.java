package com.fcott.spadger.utils;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.fcott.spadger.R;

public class ProgressBarUtils {

    private static final int progressId = R.id.progressbar_id;

    public static void showProgressBar(Activity activity){
        if(null == activity){
            return;
        }

        ViewGroup viewGroup = (ViewGroup) activity.getWindow().getDecorView();
        if(null != viewGroup){
            if(null != viewGroup.findViewById(progressId)){
                View progressBar = viewGroup.findViewById(progressId);
                progressBar.setVisibility(View.VISIBLE);
            }else{
                View view = View.inflate(activity, R.layout.layout_progressbar, null);
                view.setId(progressId);
                viewGroup.addView(view);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;
                view.setLayoutParams(params);
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    public static void hideProgressBar(Activity activity){
        if(null == activity){
            return;
        }
        ViewGroup viewGroup = (ViewGroup) activity.getWindow().getDecorView();
        if(null != viewGroup){
            if(null != viewGroup.findViewById(progressId)){
                View progressBar = viewGroup.findViewById(progressId);
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    public static boolean isShowing(Activity activity){
        if(null == activity){
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) activity.getWindow().getDecorView();
        if(null != viewGroup){
            if(null != viewGroup.findViewById(progressId)){
                View progressBar = viewGroup.findViewById(progressId);
                return progressBar.getVisibility() == View.VISIBLE;
            }
        }
        return false;
    }
}
