package com.fcott.spadger.utils.imageUtils.bean;

import android.graphics.drawable.Drawable;

public class SimpleImageOpConfiger {

    /***
     * 是否 缓存到缓存中
     */
    public boolean isCacheInMemory = true;

    /***
     * 是否 缓存到缓SD中
     */
    public boolean isCacheOnDisk = true;

    /***
     * 错误 显示的图片
     */
    public int errorPic;

    /***
     * 加载中 显示的图片
     */
    public int loadingPic;

    /***
     * 错误 显示的图片
     */
    public Drawable errorDrawable;

    /***
     * 加载中 显示的图片
     */
    public Drawable loadingDrawable;

    /***
     * 圆角
     */
    public int radius = -1;
    /**
     * 图片宽度
     */
    public int width;
    /**
     * 图片的高度
     */
    public int heigt;
    /**
     * 设置 true解决gif背景变绿 问题  正常的为false
     */
    public boolean isCencerImage;
    /**
     * 解决CenterCrop和圆角不兼容的问题
     */
    public boolean needCenterCrop;
    /**
     * 缩略图 例如，你传了0.1f作为参数，那么Glide则会显示原图大小的10%。
     if (ops.thumbnail >= 0) {
         if (ops.thumbnail == 0) {
            builder.thumbnail();
         } else {
            builder.thumbnail(ops.thumbnail);
         }
     }
     */
    public float thumbnail = -1f;
}
