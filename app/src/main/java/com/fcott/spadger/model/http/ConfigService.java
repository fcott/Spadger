package com.fcott.spadger.model.http;

import retrofit2.http.GET;
import rx.Observable;


public interface ConfigService {
    public String BASE_URL = "http://176.122.152.79/";

    @GET("/config.json")
    Observable<String> getConfig();
}
